module.exports = {
  internal: {
    notifications: {
      host: process.env.LISTEN_ADDRESS || "0.0.0.0",
    },
  },
};
